<div class="content-comment">
<section id="comments-section" class="comments <?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($content['comments'] && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <h3 class="comments-title"><?php print 'Comments (' . $node->comment_count . ')'; ?></h3><hr>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <div id="comments" class="comment-wrapper comment-wrapper-nid-<?php print $node->nid?> comments-list"><?php print render($content['comments']); ?></div>

  <?php if ($content['comment_form']): ?>
    <h3 id="reply-title" class="comment-reply-title"><?php print t('Leave a comment'); ?></h3>
    <?php print render($content['comment_form']); ?>
  <?php endif; ?>
</section>
</div>