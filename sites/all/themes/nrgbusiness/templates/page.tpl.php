<?php
  $base_path = base_path();
  $path_to_theme = drupal_get_path('theme', 'nrgbusiness');
?>
<header class="generic-header header">
  <div class="container clearfix nopadding">
    <div id="logo">
      <a href="<?php print $front_page; ?>">
        <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>">
      </a>
    </div>
    <?php if ($page['main_menu']): ?>
      <div class="menu-button  a-menu-button">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <nav class="nav ag-nav  clearfix">
        <?php print render($page['main_menu']); ?>
      </nav>
    <?php endif; ?>
  </div>
</header>
<div class="page-title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="block-title"><?php print $title; ?></h2>
        <?php if( !empty($node) && $node->type == 'article'): ?>
        <p class="sub-title"><span class="post-meta"><?php print $node->name; ?>  |  <?php print format_date($node->created, 'custom', 'F j / Y'); ?></span></p>
        <?php endif; ?>				
      </div>
    </div>
  </div>
</div>

<div id="ccn_dConsole">
  <div class="container">
      <?php if ($messages): ?>
        <div id="messages">
          <?php print $messages; ?>
        </div><!-- /#messages -->
      <?php endif; ?>
      <?php if ($tabs_rendered = render($tabs)): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
    <?php if ($feed_icons): ?>
      <div class="container">
        <?php print $feed_icons; ?>
      </div>
    <?php endif; ?>
  </div>
</div><!-- /#ccn_dConsole -->

<div class="blog blog-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
       <?php if( !empty($node) && $node->type == 'article'): ?>
        <div class="row">
          <div class="col-md-12 col-sm-12 single-content post">
       <?php endif; ?>
            <?php print render($page['content']); ?>
<?php if( !empty($node) && $node->type == 'article'): ?>
          </div>
        </div>
    <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<footer class="footer b-footer">
  <div class="container container-footer">
    <a class="logo-footer" href="<?php print $front_page; ?>">
      <img src="<?php if (theme_get_setting('onepage_region_footer_logo')): ?><?php print $footer_logo_img; ?><?php else: ?><?php print $base_path . $path_to_theme; ?>/images/business/footer-logo.png<?php endif; ?>" alt="logo"/>
    </a>
    <?php if (theme_get_setting('onepage_region_footer_footer_text')): ?>
      <p class="footer-info s-footer-info">
        <?php print (theme_get_setting('onepage_region_footer_footer_text')); ?>
      </p>
    <?php endif; ?>
    <?php if($page['footer']): ?>
      <?php print render($page['footer']); ?>
    <?php endif; ?>
  </div>
  <?php if (theme_get_setting('footer_menu')): ?>
    <div class="ftr-nav-container">
      <div class="container">
        <nav class="ftr-nav b-ftr-nav clearfix">
          <?php if ($page['main_menu']): ?>
            <?php print render($page['main_menu']); ?>
          <?php endif; ?>
        </nav>
      </div>
    </div>
  <?php endif; ?>
</footer>
