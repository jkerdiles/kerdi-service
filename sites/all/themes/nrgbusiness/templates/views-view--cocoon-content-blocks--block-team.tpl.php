<?php
$base_path = base_path();
$path_to_theme = drupal_get_path('theme', 'nrgbusiness');
?>
<div class="team b-team scroll-to-block"><div class="container">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <h2 class="block-title b-block-title">
      <?php print $header; ?>
    </h2>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="row">
      <div class="swiper-container" data-mode="horizontal" data-slides-per-view="responsive" data-xs-slides="1" data-sm-slides="2" data-md-slides="4" data-lg-slides="4" data-loop="0">
        <div class="swiper-wrapper">
          <?php print $rows; ?>
          <div class="pagination swiper-pagination">
          </div>
        </div>
      </div>
      <div class="arrows a-arrow">
        <div class="arrow-left"><img src="<?php print $base_path . $path_to_theme; ?>/images/agency/arrow-l.png" alt="arrow"></div>
        <div class="arrow-right"><img src="<?php print $base_path . $path_to_theme; ?>/images/agency/arrow.png" alt="arrow"></div>
      </div>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>
</div></div>
