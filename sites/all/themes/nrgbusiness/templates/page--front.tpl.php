<?php
  $base_path = base_path();
  $path_to_theme = drupal_get_path('theme', 'nrgbusiness');

  // Get Cocoon theme settings
  if (theme_get_setting('onepage_region_footer_logo')):
    $footer_logo_fid = theme_get_setting('onepage_region_footer_logo');
    $footer_logo_img = file_create_url(file_load($footer_logo_fid)->uri);
  endif;
?>
<header class="b-header header">
  <div class="container clearfix nopadding">
    <div id="logo">
      <a href="<?php print $front_page; ?>">
        <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>">
      </a>
    </div>
    <?php if ($page['main_menu']): ?>
      <div class="menu-button bs-menu-button">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <nav class="nav b-nav  clearfix">
        <?php print render($page['main_menu']); ?>
      </nav>
    <?php endif; ?>
  </div>
</header>
          <?php if($page['onepage_hero']): ?>
          <?php print render($page['onepage_hero']); ?>
<?php endif;?>

<?php if ($messages): ?>
    <div class="container">
      <?php if ($messages): ?>
        <div id="messages">
          <?php print $messages; ?>
        </div><!-- /#messages -->
      <?php endif; ?>
    </div>
<?php endif; ?>
<?php if($page['onepage_services']): ?>
      <?php print render($page['onepage_services']); ?>
<?php endif; ?>

<?php if($page['onepage_history']): ?>
      <?php print render($page['onepage_history']); ?>
<?php endif; ?>

<?php if($page['onepage_work_process']): ?>
      <?php print render($page['onepage_work_process']); ?>
<?php endif; ?>

<?php if($page['onepage_projects']): ?>
      <?php print render($page['onepage_projects']); ?>
<?php endif; ?>

<?php if($page['onepage_team']): ?>
      <?php print render($page['onepage_team']); ?>
<?php endif; ?>

<?php if($page['onepage_pricing_tables']): ?>
      <?php print render($page['onepage_pricing_tables']); ?>
<?php endif; ?>

<?php if($page['onepage_skills']): ?>
      <?php print render($page['onepage_skills']); ?>
<?php endif; ?>

<?php if($page['onepage_blog']): ?>
  <div class="blog scroll-to-block"  >
    <div class="container from-blog">
      <?php print render($page['onepage_blog']); ?>
    </div>
  </div>
<?php endif; ?>

<?php if($page['onepage_contact_header'] || $page['onepage_contact_details'] || $page['onepage_contact']): ?>
  <div class="contact scroll-to-block">

      <?php if($page['onepage_contact_header']): ?>
        <div class="container">
          <?php print render($page['onepage_contact_header']); ?>
        </div>
      <?php endif; ?>

    <?php if (theme_get_setting('show_map')): ?>
        <div id="map-canvas" data-lat="<?php print (theme_get_setting('map_latitude')); ?>" data-lng="<?php print (theme_get_setting('map_longitude')); ?>" data-zoom="10" data-style="style-6">
        </div>
        <div class="addresses-block">
            <a data-lat="<?php print (theme_get_setting('map_latitude')); ?>" data-lng="<?php print (theme_get_setting('map_longitude')); ?>" data-string="<?php print (theme_get_setting('map_marker_text')); ?>"></a>
        </div>
<?php endif; ?>


      <?php if($page['onepage_contact_details'] || $page['onepage_contact']): ?>
        <div class="container contact-us"><div class="row">
          <?php if($page['onepage_contact_details']): ?>
            <div class="col-md-4 contact-info">
              <?php print render($page['onepage_contact_details']); ?>
            </div>
          <?php endif; ?>
          <?php if($page['onepage_contact']): ?>
            <div class="<?php if($page['onepage_contact_details']): ?>col-md-8<?php else: ?>col-md-12<?php endif; ?>">
              <?php print render($page['onepage_contact']); ?>
            </div>
          <?php endif; ?>
        </div></div>
      <?php endif; ?>

  </div>
<?php endif; ?>


<footer class="footer b-footer">
  <div class="container container-footer">
    <a class="logo-footer" href="<?php print $front_page; ?>">
      <img src="<?php if (theme_get_setting('onepage_region_footer_logo')): ?><?php print $footer_logo_img; ?><?php else: ?><?php print $base_path . $path_to_theme; ?>/images/business/footer-logo.png<?php endif; ?>" alt="logo"/>
    </a>
    <?php if (theme_get_setting('onepage_region_footer_footer_text')): ?>
      <p class="footer-info s-footer-info">
        <?php print (theme_get_setting('onepage_region_footer_footer_text')); ?>
      </p>
    <?php endif; ?>
    <?php if($page['footer']): ?>
      <?php print render($page['footer']); ?>
    <?php endif; ?>
  </div>
  <?php if (theme_get_setting('footer_menu')): ?>
    <div class="ftr-nav-container">
      <div class="container">
        <nav class="ftr-nav b-ftr-nav clearfix">
          <?php if ($page['main_menu']): ?>
            <?php print render($page['main_menu']); ?>
          <?php endif; ?>
        </nav>
      </div>
    </div>
  <?php endif; ?>
</footer>
