<?php
function nrgbusiness_form_system_theme_settings_alter(&$form, &$form_state) {
  // Cocoon Options
  $form['cocoon_options'] = array(
      '#type' => 'vertical_tabs',
      '#title' => t('Cocoon Theme Options'),
      '#weight' => 0,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );
  // Cocoon Theme Skin Panel
  $form['cocoon_options']['cocoon_skin'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Theme Skin'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Theme Skin Panel: Custom CSS
  $form['cocoon_options']['cocoon_skin']['custom_css'] = array(
    '#type' => 'textarea', 
    '#title' => t('Custom CSS'), 
    '#description' => t('Specify custom CSS tags and styling to apply to the theme. You can also override default styles here.'),
    '#rows' => '5',
    '#default_value' => theme_get_setting('custom_css'),
  );
  // Cocoon Theme Features Panel
  $form['cocoon_options']['cocoon_features'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Theme Features'), 
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Theme Features Panel: Preloader
  $form['cocoon_options']['cocoon_features']['preloader'] = array(
    '#type' => 'select',
    '#title' => t('Preloader'),
    '#description' => t('Display loading animation while pages load?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => theme_get_setting('preloader'),
  );
  // Cocoon Theme Features Panel: Footer Menu
  $form['cocoon_options']['cocoon_features']['footer_menu'] = array(
    '#type' => 'select',
    '#title' => t('Footer Menu'),
    '#description' => t('Display the site menu in the footer, as well as the header?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => theme_get_setting('footer_menu'),
  );
  // Cocoon Onepage Regions
  $form['cocoon_options']['cocoon_onepage_regions'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Page Settings'), 
    '#description' => t('Change various page settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon One-Page Regions: Projects
  $form['cocoon_options']['cocoon_onepage_regions']['projects'] = array(
    '#type' => 'fieldset',
    '#title' => 'Onepage: Projects',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Cocoon One-Page Regions: Projects: Background Image
  $form['cocoon_options']['cocoon_onepage_regions']['projects']['onepage_region_projects_bg_img'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Projects Background Image'),
    '#description'   => t('Upload a background image for the Onepage: Projects section.'),
    '#required' => FALSE,
    '#upload_location' => 'public://backgrounds',
    '#default_value' => theme_get_setting('onepage_region_projects_bg_img'), 
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  // Cocoon One-Page Regions: Skills
  $form['cocoon_options']['cocoon_onepage_regions']['skills'] = array(
    '#type' => 'fieldset',
    '#title' => 'Onepage: Skills',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Cocoon One-Page Regions: Skills: Background Image
  $form['cocoon_options']['cocoon_onepage_regions']['skills']['onepage_region_skills_bg_img'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Skills Background Image'),
    '#description'   => t('Upload a background image for the Onepage: Skills section.'),
    '#required' => FALSE,
    '#upload_location' => 'public://backgrounds',
    '#default_value' => theme_get_setting('onepage_region_skills_bg_img'), 
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  // Cocoon One-Page Regions: Footer
  $form['cocoon_options']['cocoon_onepage_regions']['footer'] = array(
    '#type' => 'fieldset',
    '#title' => 'Footer',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Cocoon One-Page Regions: Footer: Logo
  $form['cocoon_options']['cocoon_onepage_regions']['footer']['onepage_region_footer_logo'] = array(
    '#type'     => 'managed_file',
    '#title'    => t('Footer logo'),
    '#description'   => t('Upload a logo for the footer'),
    '#required' => FALSE,
    '#upload_location' => 'public://',
    '#default_value' => theme_get_setting('onepage_region_footer_logo'), 
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
  );
  // Cocoon One-Page Regions: Footer: Footer text
  $form['cocoon_options']['cocoon_onepage_regions']['footer']['onepage_region_footer_footer_text'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Footer text'),
    '#rows' => '5',
    '#default_value' => theme_get_setting('onepage_region_footer_footer_text'),
    '#description'   => t("Enter the footer text. HTML is allowed."),
  );
  // Cocoon Map Settings
  $form['cocoon_options']['cocoon_map_settings'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Cocoon Map Settings'), 
    '#description' => t('Change map settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Cocoon Theme Features Panel: Footer Menu
  $form['cocoon_options']['cocoon_map_settings']['show_map'] = array(
    '#type' => 'select',
    '#title' => t('Show Map?'),
    '#description' => t('Display the Google Map on the homepage?'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => theme_get_setting('show_map'),
  );
  // Cocoon Map Settings: Latitude
  $form['cocoon_options']['cocoon_map_settings']['map_latitude'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Latitude'),
    '#default_value' => theme_get_setting('map_latitude'),
    '#description'   => t("Latitude for the map address."),
  );
  // Cocoon Map Settings: Longitude
  $form['cocoon_options']['cocoon_map_settings']['map_longitude'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Longitude'),
    '#default_value' => theme_get_setting('map_longitude'),
    '#description'   => t("Longitude for the map address."),
  );
  // Cocoon Map Settings: Marker Text
  $form['cocoon_options']['cocoon_map_settings']['map_marker_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Map Marker Text'),
    '#default_value' => theme_get_setting('map_marker_text'),
    '#description'   => t("Text to display when map marker is clicked."),
  );


  $form['#submit'][] = 'nrgbusiness_settings_form_submit';

  // Get all themes.
  $themes = list_themes();

  // Get the current theme
  $active_theme = $GLOBALS['theme_key'];
  $form_state['build_info']['files'][] = str_replace("/$active_theme.info", '', $themes[$active_theme]->filename) . '/theme-settings.php';

}

function nrgbusiness_settings_form_submit(&$form, $form_state) {

  $onepage_region_projects_bg_img_image_fid = $form_state['values']['onepage_region_projects_bg_img'];
  $onepage_region_projects_bg_img_image = file_load($onepage_region_projects_bg_img_image_fid);
  if (is_object($onepage_region_projects_bg_img_image)) {
    // Check to make sure that the file is set to be permanent.
    if ($onepage_region_projects_bg_img_image->status == 0) {
      // Update the status.
      $onepage_region_projects_bg_img_image->status = FILE_STATUS_PERMANENT;
      // Save the update.
      file_save($onepage_region_projects_bg_img_image);
      // Add a reference to prevent warnings.
      file_usage_add($onepage_region_projects_bg_img_image, 'nrgbusiness', 'theme', 1);
    }
  }

 $onepage_region_skills_bg_img_image_fid = $form_state['values']['onepage_region_skills_bg_img'];
  $onepage_region_skills_bg_img_image = file_load($onepage_region_skills_bg_img_image_fid);
  if (is_object($onepage_region_skills_bg_img_image)) {
    // Check to make sure that the file is set to be permanent.
    if ($onepage_region_skills_bg_img_image->status == 0) {
      // Update the status.
      $onepage_region_skills_bg_img_image->status = FILE_STATUS_PERMANENT;
      // Save the update.
      file_save($onepage_region_skills_bg_img_image);
      // Add a reference to prevent warnings.
      file_usage_add($onepage_region_skills_bg_img_image, 'nrgbusiness', 'theme', 1);
    }
  }

}

?>